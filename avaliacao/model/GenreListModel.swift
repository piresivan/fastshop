import UIKit
import ObjectMapper

open class GenreListModel :Mappable {
    var id               : Int?
    var name             : String?
    
    required public init?(map: Map) {
    }
    
    public init() {
    }
    
    open func mapping(map: Map) {
        id               <- map["id"]
        name             <- map["name"]
    }
}
