import UIKit
import ObjectMapper

open class VideoListModel :Mappable {
    var id         : String?
    var iso_639_1  : String?
    var iso_3166_1 : String?
    var key        : String?
    var name       : String?
    var site       : String?
    var size       : Int?
    var type       : String?
    
    required public init?(map: Map) {
    }
    
    public init() {
    }
    
    open func mapping(map: Map) {
        id         <- map["id"]
        iso_639_1  <- map["iso_639_1"]
        iso_3166_1 <- map["iso_3166_1"]
        key        <- map["key"]
        name       <- map["name"]
        site       <- map["site"]
        size       <- map["size"]
        type       <- map["type"]
    }
}
