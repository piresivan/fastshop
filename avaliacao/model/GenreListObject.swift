import Foundation
import ObjectMapper

open class GenreListObject : Mappable {
    
    var items             : [GenreListModel]?
    
    public required init?(map: Map) {
    }
    
    public init() {
    }
    
    open func mapping(map: Map) {
        items              <- map["genres"]
    }
}
