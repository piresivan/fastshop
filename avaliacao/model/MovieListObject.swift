import Foundation
import ObjectMapper

open class MovieListObject: Mappable {
    
    var page    : Int?
    var results : [MovieListModel]?
    var total_results : Int?
    var total_pages : Int?
    
    public required init?(map: Map) {
    }
    
    public init() {
    }
    
    open func mapping(map: Map) {
        page    <- map["page"]
        results <- map["results"]
        total_results <- map["total_results"]
        total_pages <- map["total_pages"]
    }
}
