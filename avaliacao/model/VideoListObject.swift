import Foundation
import ObjectMapper

open class VideoListObject : Mappable {
    
    var items : [VideoListModel]?
    var id    : Int?
    
    public required init?(map: Map) {
    }
    
    public init() {
    }
    
    open func mapping(map: Map) {
        items              <- map["results"]
        id                 <- map["id"]
    }
}
