import UIKit
import ObjectMapper
import UIScrollView_InfiniteScroll
import KRProgressHUD

class GenreViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    
    var listGenre = GenreListObject()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationBar()
        
        getRepositories()
        tableViewConfiguration()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableViewConfiguration() {
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.register(UINib(nibName: "GenreCell", bundle: nil), forCellReuseIdentifier: "GenreCell")
        
        tableView.backgroundColor = UIColor("#202346")
        
        tableView.allowsSelection = true
        tableView.separatorStyle = .none
    }
    
    func setupNavigationBar () {
        self.title = "Genre List"
        self.navigationController?.navigationBar.barStyle = .black
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationItem.leftBarButtonItem = nil
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    }
    
    func getRepositories() {
        let service = TheMovieDBService()
        
        KRProgressHUD.show(withMessage: "Loading...")
        service.getGenreList(success: { (repositorieObject) in
            
            KRProgressHUD.dismiss()
            if(self.listGenre.items == nil) {
                self.listGenre = repositorieObject
            }
            else {
                self.listGenre.items = repositorieObject.items!
            }
            
            self.tableView.reloadData()
            
        }) { (error) in
            KRProgressHUD.dismiss()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "seguePushToPullRequest") {
            let moviesViewController = segue.destination as! MoviesViewController
            
            let row: Int = (self.tableView?.indexPathForSelectedRow)!.row
            
            moviesViewController.listGenre = self.listGenre.items![row]
            
            self.tableView?.deselectRow(at: (self.tableView?.indexPathForSelectedRow)!, animated: true)
        }
    }
}

extension GenreViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(listGenre.items != nil) {
            return (listGenre.items?.count)!
        }
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 61
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "GenreCell", for: indexPath) as! GenreCell
        
        cell.cellConfiguration(listGenre.items![indexPath.row])
        cell.backgroundColor = UIColor("#202346")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let MoviesViewController = self.storyboard?.instantiateViewController(withIdentifier: "MoviesViewController") as! MoviesViewController
        
        MoviesViewController.listGenre = self.listGenre.items![indexPath.row]
        
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        self.navigationController!.pushViewController(MoviesViewController, animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}
