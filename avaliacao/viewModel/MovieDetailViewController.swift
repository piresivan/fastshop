import UIKit
import KRProgressHUD

class MovieDetailViewController: UIViewController {
    var movieDetail = MovieDetailObject()
    var idMovie : Int?
    
    @IBOutlet var imgPoster: UIImageView!
    @IBOutlet var thumbnail: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblOverview: UILabel!
    @IBOutlet weak var lblUserScore: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        getDetail()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setupNavigationBar () {
        self.title = "Movie"
        self.navigationController?.navigationBar.barStyle = .black
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationItem.leftBarButtonItem = nil
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        self.view.backgroundColor = UIColor("#202346")
    }
    
    func getDetail() {
        let service = TheMovieDBService()
        
        KRProgressHUD.show(withMessage: "Loading...")
        
        service.getMovieDetail(self.idMovie!, success: { (requestObject) in
            
            self.movieDetail = requestObject
            
            if (requestObject.poster_path != nil) {
                let image_url_string = "http://image.tmdb.org/t/p/w185" + requestObject.poster_path!
                let url = URL(string: image_url_string)
                self.downloadImage(url: url!)
            }
            
            if (requestObject.backdrop_path != nil) {
                let image_url_string = "http://image.tmdb.org/t/p/w185" + requestObject.backdrop_path!
                let url = URL(string: image_url_string)
                self.downloadThumbnail(url: url!)
            }
            
            self.lblTitle.text = requestObject.title
            self.lblOverview.text = requestObject.overview
            
            let average = String(Int(requestObject.vote_average! * 10))
            
            self.lblUserScore.text = "\(average)% User Score"

            let string1 = requestObject.release_date
            let index1 = string1?.index((string1?.endIndex)!, offsetBy: -6)
            let year = string1![..<index1!]
            
            self.lblDate.text = "(\(year))"

            KRProgressHUD.dismiss()
        }) { (error) in
            KRProgressHUD.dismiss()
        }
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
            }.resume()
    }
    
    func downloadImage(url: URL) {
        getDataFromUrl(url: url) { data, response, error in
            guard let data = data, error == nil else { return }
            DispatchQueue.main.async() {
                self.imgPoster.image = UIImage(data: data)
            }
        }
    }
    @IBAction func playTrailer(_ sender: Any) {
        let service = TheMovieDBService()
        
        KRProgressHUD.show(withMessage: "Loading...")
        
        service.getVideo(self.idMovie!, success: { (requestObject) in
            
            if (requestObject.items != nil) {
                let youtubeId = String(describing: requestObject.items![0].key!)
                let url = URL(string:"http://www.youtube.com/watch?v=\(youtubeId)")!
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
            
            KRProgressHUD.dismiss()
        }) { (error) in
            KRProgressHUD.dismiss()
        }
    }
    
    func downloadThumbnail(url: URL) {
        getDataFromUrl(url: url) { data, response, error in
            guard let data = data, error == nil else { return }
            DispatchQueue.main.async() {
                self.thumbnail.image = UIImage(data: data)
            }
        }
    }

}
