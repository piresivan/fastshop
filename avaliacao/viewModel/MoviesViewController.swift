import UIKit
import KRProgressHUD

class MoviesViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    
    var listGenre   = GenreListModel()
    var listMovie   = MovieListObject()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationBar()
        getPullRequests()
        tableViewConfiguration()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableViewConfiguration() {
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.register(UINib(nibName: "MoviesCell", bundle: nil), forCellReuseIdentifier: "MoviesCell")
        
        tableView.backgroundColor = UIColor("#202346")
        
        tableView.allowsSelection = true
        tableView.separatorStyle = .none
    }
    
    func setupNavigationBar () {
        self.title = self.listGenre.name
        self.navigationController?.navigationBar.barStyle = .black
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationItem.leftBarButtonItem = nil
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    }
    
    func getPullRequests() {
        let service = TheMovieDBService()
        
        KRProgressHUD.show(withMessage: "Loading...")
        
        service.getMovieList((self.listGenre.id)!, success: { (requestObject) in
            
            KRProgressHUD.dismiss()
            
            if(self.listMovie.results?.count == 0) {
                self.listMovie = requestObject
            }
            else {
                self.listMovie.results = requestObject.results!
            }
            
            self.tableView.reloadData()
            
        }) { (error) in
            KRProgressHUD.dismiss()
        }
    }
}

extension MoviesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(self.listMovie.results?.count != nil) {
            return (self.listMovie.results!.count)
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MoviesCell", for: indexPath) as! MoviesCell
        
        cell.cellConfiguration(self.listMovie.results![indexPath.row])
        cell.backgroundColor = UIColor("#202346")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let movieDetailViewController = self.storyboard?.instantiateViewController(withIdentifier: "MovieDetailViewController") as! MovieDetailViewController
        movieDetailViewController.idMovie = self.listMovie.results![indexPath.row].id
        self.navigationController!.pushViewController(movieDetailViewController, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 118
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}
