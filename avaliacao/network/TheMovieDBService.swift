import UIKit
import Alamofire
import ObjectMapper

class TheMovieDBService: NSObject {
    
    func getGenreList(success: ((_ objectReturn: GenreListObject) -> Void)?, failure: ((NSError?) -> Void)?) {
        
        let APIKey: String = Utils.getConfigurationValueForKey("api")
        let url = "https://api.themoviedb.org/3/genre/movie/list"
        
        AF.request( url , method: .get, parameters: ["api_key": APIKey]).responseJSON { response in
            let repositorie = Mapper<GenreListObject>().map(JSONObject:response.value)
            
            if (repositorie != nil) {
                success!(repositorie!)
            }
        }
    }
    
    func getMovieList(_ id: Int, success: ((_ objectReturn: MovieListObject) -> Void)?, failure: ((NSError?) -> Void)?) {
        
        let APIKey: String = Utils.getConfigurationValueForKey("api")
        let url = "https://api.themoviedb.org/3/discover/movie"
        
        AF.request(url, method: .get, parameters: ["api_key": APIKey, "with_genres":id] ).responseJSON { response in
            let repositorie = Mapper<MovieListObject>().map(JSONObject: response.value)
            
            if (repositorie != nil) {
                success!(repositorie!)
            }
        }
    }
    
    func getMovieDetail(_ id: Int, success: ((_ objectReturn: MovieDetailObject) -> Void)?, failure: ((NSError?) -> Void)?) {
        
        let APIKey: String = Utils.getConfigurationValueForKey("api")
        let url = "https://api.themoviedb.org/3/movie/\(id)"
        
        AF.request(url, method: .get, parameters: ["api_key": APIKey] ).responseJSON { response in
            let repositorie = Mapper<MovieDetailObject>().map(JSONObject:response.value)
            
            if (repositorie != nil) {
                success!(repositorie!)
            }
        }
    }
    
    func getVideo(_ id: Int, success: ((_ objectReturn: VideoListObject) -> Void)?, failure: ((NSError?) -> Void)?) {
        
        let APIKey: String = Utils.getConfigurationValueForKey("api")
        let url = "https://api.themoviedb.org/3/movie/\(id)/videos"
        
        AF.request(url, method: .get, parameters: ["api_key": APIKey] ).responseJSON { response in
            let repositorie = Mapper<VideoListObject>().map(JSONObject:response.value)
            
            if (repositorie != nil) {
                success!(repositorie!)
            }
        }
    }
}
