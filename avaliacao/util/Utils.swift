import UIKit

class Utils: NSObject {
    
    class func getConfigurationValueForKey(_ key: String) -> String {
        let path = Bundle.main.path(forResource: "Configuration", ofType: "plist")
        let dict = NSDictionary(contentsOfFile: path!)
        
        let value = (dict?.object(forKey: key))
        
        return value as! String
    }
}
