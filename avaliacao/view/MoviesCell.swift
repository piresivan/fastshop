import UIKit
import Alamofire

class MoviesCell: UITableViewCell {
    
    @IBOutlet var title: UILabel!
    @IBOutlet var sinopse: UILabel!
    @IBOutlet var thumbnail: UIImageView!
    @IBOutlet var detail: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        configuration()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configuration() {
        title.textColor = UIColor.white
        sinopse.textColor = UIColor.white
        detail.textColor = UIColor.white
    }
    
    func cellConfiguration(_ object: MovieListModel) {
        var releaseDate = ""
        var rating      = ""
        
        if (object.vote_average != nil) {
            rating = String(describing: object.vote_average! as Double)
        }
        
        if (object.release_date != nil) {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let stringDate = formatter.string(from: object.release_date!)
            releaseDate = "Release " + stringDate
        }
        
        if (object.poster_path != nil) {
            let image_url_string = "http://image.tmdb.org/t/p/w185" + object.poster_path!
            let image_from_url_request = URL(string: image_url_string)
            downloadImage(url: image_from_url_request!)
        }
        
        title.text   = object.title
        sinopse.text = object.overview
        detail.text  = "Rating \(rating)\(releaseDate)"
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
            }.resume()
    }
        
    func downloadImage(url: URL) {
        getDataFromUrl(url: url) { data, response, error in
            guard let data = data, error == nil else { return }
            DispatchQueue.main.async() {
                self.thumbnail.image = UIImage(data: data)
            }
        }
    }
}
