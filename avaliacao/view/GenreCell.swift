import UIKit
import Alamofire
import UIColor_Hex_Swift

class GenreCell: UITableViewCell {
    
    @IBOutlet var repositorieNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        configuration()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configuration() {
        repositorieNameLabel.adjustsFontSizeToFitWidth = true
        repositorieNameLabel.textColor = UIColor.white
    }
    
    func cellConfiguration(_ repositorieItem: GenreListModel) {
        repositorieNameLabel.text = repositorieItem.name
        
    }
}

