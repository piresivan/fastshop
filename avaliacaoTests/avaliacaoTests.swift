import XCTest
@testable import avaliacao

class avaliacaoTests: XCTestCase {
    
    var listGenre = GenreListObject()
    
    override func setUp() {
        super.setUp()
        getRepositories()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testExample() {
    }
    
    func testPerformanceExample() {
        self.measure {
            XCTAssertNotNil(listGenre)
        }
    }
    
    func getRepositories() {
        let service = TheMovieDBService()
        
        service.getGenreList(success: { (repositorieObject) in
            
            if(self.listGenre.items == nil) {
                self.listGenre = repositorieObject
            }
            else {
                self.listGenre.items = repositorieObject.items!
            }
            
        }) { (error) in
        }
    }
}
